import createApiSpec from '~/apiSpecs'
import {Song} from './song.model'
import {User} from "../user/user.model";
import {runQuery, dropDb} from '../../../../test/helpers'
import {expect} from 'chai'

createApiSpec(
    Song,
    'song',
    {title: 'downtown jamming', url: 'http://music.mp3'}
)
describe('Song', () => {
    let user
    beforeEach(async () => {
        await dropDb()
        user = await User.create({username: 'stu1', passwordHash: '123'})
    })

    afterEach(async () => {
        await dropDb()
    })

    it('should create a Song', async () => {
        const result = await runQuery(`
            mutation CreateSong($input: NewSong!) {
                newSong(input: $input) {
                    id
                    title
                }
            }
        `, {
            input: {
                title: 'Drop Down',
                url: 'http://dropDown.mp3',
                artist: 'JJ'
            }
        }, user)
        
        expect(result.error).to.not.exist
        expect(result.data.newSong).to.exist
        expect(result.data.newSong.title).to.equal('Drop Down')
    })
})
